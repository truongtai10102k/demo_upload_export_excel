const express = require("express");
const mongoose = require("mongoose")
const logger = require("morgan")
const app = express();
const cors = require('cors')

app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(cors())
app.use(logger("dev"));

mongoose.connect("mongodb://localhost:27017/excel",
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err) => {
        if(err) console.log("connect to dabase error ", err);
        else{
            console.log("connect to database success");
        }
    })


const PORT = 3001;
const router = require("./API/router")

app.use("/api", router)



app.listen(PORT, (err) => {
    if (err) console.log("server running error : ", err);
    else {
        console.log("server running on port " + PORT);
    }
})