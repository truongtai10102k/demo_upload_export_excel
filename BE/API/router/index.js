const express = require("express");
const Router = express.Router();
const controller = require("../controller")
const midlewareExcel = require("../midleware")



Router.route("/user")
    .get(controller.getData)
    .post(controller.postData)
Router.route("/user/:id")
    .put(controller.putData)
    .delete(controller.deleteData)

Router.route("/getAll")
    .get(controller.getAll)


Router.route("/upload")
    .post(midlewareExcel.single("uploadfile"), controller.postExcel)

module.exports = Router