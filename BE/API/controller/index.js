const xlsx = require("node-xlsx")
const fs = require('fs')
const ExcelModel = require("../model/index")
const path = require('path')

exports.getData = async (req, res) => {
    let { page, limit } = req.query;
    let skip = (+page - 1) * (+limit)
    try {
        let result = await ExcelModel.find({}).limit(+limit).skip(skip);
        let totalData = await ExcelModel.countDocuments();
        console.log("data ", totalData);
        let totalPage = Math.ceil(totalData / (+limit))

        res.json({ data: result, totalPage })
    } catch (error) {
        res.json({ message: error.message })
    }
}

exports.getAll = async (req, res) => {
    try {
        let result = await ExcelModel.find({}, { _id: 0, __v: 0 })

        res.json({ data: result })
    } catch (error) {
        res.json({ message: error.message })
    }
}

exports.putData = async (req, res) => {
    try {
        let _id = req.params.id
        console.log("id", _id);
        let { id, name, gender } = req.body;
        let updateUser = await ExcelModel.findById(_id)

        updateUser.id = id;
        updateUser.name = name;
        updateUser.gender = gender;

        let result = await updateUser.save()

        res.json({ data: result })
    } catch (error) {
        res.json({ message: error.message })
    }
}

exports.deleteData = async (req, res) => {
    try {
        let _id = req.params.id
        let deleteUser = await ExcelModel.findById(_id)

        await deleteUser.delete()

        res.json({ message: "delete sucess" })
    } catch (error) {
        res.json({ message: error.message })
    }
}

exports.postData = async (req, res) => {
    try {
        let id = req.body.id;
        let name = req.body.name;
        let gender = req.body.gender;
        console.log("id", id);
        console.log("id", name);
        console.log("id", gender);
        let newUser = ExcelModel({
            id, name, gender
        })

        let result = await newUser.save()

        res.json({ data: result })
    } catch (error) {
        res.json({ message: error.message })
    }
}






// import excel
exports.postExcel = async (req, res) => {
    try {
        const pathFile = path.join(__dirname, '..\\midleware\\uploads', req.file.originalname);

        var obj = xlsx.parse(fs.readFileSync(pathFile))

        let arrData = [];
        let header = obj[0].data[0];

        for (let i = 1; i < obj[0].data.length; i++) {
            let data = {};
            for (let j = 0; j < obj[0].data[i].length; j++) {
                data[header[j]] = obj[0].data[i][j]
            }
            arrData.push(data)
        }

        await ExcelModel.insertMany(arrData)
            .then(function (docs) {
                res.json(docs);
            })
            .catch(function (err) {
                res.json({ message: err.message })
            });
    } catch (error) {
        res.json({ message: error.message })
    }

}