const mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    model = mongoose.model

const ExcelSchema = new Schema({
    id: String,
    name: String,
    gender: String
})

module.exports = model("excel", ExcelSchema)