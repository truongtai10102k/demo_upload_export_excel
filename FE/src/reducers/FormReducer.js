import * as types from '../constants';
const DEFAULT_STATE = {
    isFetching: false,
    error: false,
    errorMessage: "",
    listData: [],
    dataFetched: false,
    activePage: 0,
    totalPage: 0
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.UPLOAD_FILE_REQUEST:
        case types.GET_ITEM_REQUEST:
        case types.POST_ITEM_REQUEST:
        case types.PUT_ITEM_REQUEST:
        case types.DELETE_ITEM_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.GET_ITEM_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                listData: action.payload.data,
                totalPage: action.payload.totalPage,
                activePage: action.payload.activePage
            }
        case types.UPLOAD_FILE_SUCCESS:
        case types.POST_ITEM_SUCCESS:
        case types.PUT_ITEM_SUCCESS:
        case types.DELETE_ITEM_SUCCESS:
            return {
                ...state,
                isFetching: false
            }
        case types.UPLOAD_FILE_FAILURE:
        case types.GET_ITEM_FAILURE:
        case types.POST_ITEM_FAILURE:
        case types.PUT_ITEM_FAILURE:
        case types.DELETE_ITEM_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            }

        default:
            return {
                ...state
            }
    }
}