import { all } from 'redux-saga/effects';
import { LoginSaga } from "./FormSaga"
export default function* rootSaga() {
  yield all([
    ...LoginSaga
  ]);
}