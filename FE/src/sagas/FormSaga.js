import { takeEvery, put, select } from "redux-saga/effects"
import * as types from "../constants"
import excelAPI from '../fetchAPIs/uploadExcel'
import callAPI from '../fetchAPIs/callAPI'
function* getHandle(action) {
    try {
        const result = yield callAPI(types.HTTP_READ, `/user?page=${action.payload.page}&limit=2`, "");
        yield put({
            type: types.GET_ITEM_SUCCESS,
            payload: {
                data: result.data,
                totalPage: result.totalPage,
                activePage: action.payload.page
            }
        })

    } catch (error) {
        yield put({
            type: types.GET_ITEM_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
function* postHandle(action) {
    try {
        yield callAPI(types.HTTP_CREATE, `/user`, action.payload);
        yield put({
            type: types.POST_ITEM_SUCCESS,
        })
        yield put({
            type: types.GET_ITEM_REQUEST,
            payload: {
                page: 1
            }
        })

    } catch (error) {
        yield put({
            type: types.POST_ITEM_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
function* putHandle(action) {
    try {
        yield callAPI(types.HTTP_UPDATE, `/user/${action.payload.id}`, action.payload);
        const activePage = yield select((state) => state.Form.activePage)
        yield put({
            type: types.PUT_ITEM_SUCCESS,
        })
        yield put({
            type: types.GET_ITEM_REQUEST,
            payload: {
                page: activePage
            }
        })

    } catch (error) {
        yield put({
            type: types.POST_ITEM_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
function* deleteHandle(action) {
    try {
        yield callAPI(types.HTTP_DELETE, `/user/${action.payload}`, "");
        yield put({
            type: types.DELETE_ITEM_SUCCESS,
        })
        yield put({
            type: types.GET_ITEM_REQUEST,
            payload: {
                page: 1
            }
        })

    } catch (error) {
        yield put({
            type: types.DELETE_ITEM_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
function* uploadHandle(action) {
    try {
        const result = yield excelAPI(types.HTTP_CREATE, "/upload", action.payload);
        yield put({
            type: types.UPLOAD_FILE_SUCCESS,
            payload: result
        })
        yield put({
            type: types.GET_ITEM_REQUEST,
            payload: {
                page: 1
            }
        })

    } catch (error) {
        yield put({
            type: types.UPLOAD_FILE_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}

export const LoginSaga = [
    takeEvery(types.UPLOAD_FILE_REQUEST, uploadHandle),
    takeEvery(types.GET_ITEM_REQUEST, getHandle),
    takeEvery(types.POST_ITEM_REQUEST, postHandle),
    takeEvery(types.PUT_ITEM_REQUEST, putHandle),
    takeEvery(types.DELETE_ITEM_REQUEST, deleteHandle),
]