import React, { Component } from 'react';
import FormContainer from "../containers/Form"
class HomePage extends Component {
  render() {
    return (
      <div className="HomePage">
        <FormContainer />
      </div>
    );
  }
}

export default HomePage;
