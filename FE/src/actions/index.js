import * as types from '../constants';


export const uploadFileRequest = (payload) => {
    return({
        type : types.UPLOAD_FILE_REQUEST,
        payload
    })
}

export const getItemRequest = (payload) => {
    return({
        type : types.GET_ITEM_REQUEST,
        payload
    })
}

export const postItemRequest = (payload) => {
    return({
        type : types.POST_ITEM_REQUEST,
        payload
    })
}

export const putItemRequest = (payload) => {
    return({
        type : types.PUT_ITEM_REQUEST,
        payload
    })
}

export const deleteItemRequest = (payload) => {
    return({
        type : types.DELETE_ITEM_REQUEST,
        payload
    })
}