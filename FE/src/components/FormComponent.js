import React, { useState, useEffect } from 'react'
import * as xlsx from 'xlsx';
import FileSaver from 'file-saver';
import * as types from '../constants'
const FormComponent = (props) => {
    const { upload } = props

    const [file, setFile] = useState();
    const [name, setName] = useState("");
    const [gender, setGender] = useState("");
    const [id, setId] = useState(0);

    const saveFile = (e) => {
        setFile(e.target.files[0]);
    };

    const uploadFile = async (e) => {
        const formData = new FormData();
        await formData.append("uploadfile", file);
        upload(formData)

    };
    // function export excel
    const ExportExcel = (csvData, fileName) => {
        const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        const fileExtension = '.xlsx';
        const ws = xlsx.utils.json_to_sheet(csvData);
        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
        const excelBuffer = xlsx.write(wb, { bookType: 'xlsx', type: 'array' });
        const data = new Blob([excelBuffer], { type: fileType });
        FileSaver.saveAs(data, fileName + fileExtension);
    }

    // handle export excel

    const exportFile = async () => {
        console.log("vào ");
        const dataAll = await new Promise((resolve, reject) => {
            let objFetch = {
                method: types.HTTP_READ
            }
            const url = types.DOMAIN + "/getAll"
            fetch(url, objFetch)
                .then((response) => resolve(response.json()))
                .catch((error) => reject(error));
        })
        await ExportExcel(dataAll.data, "excel")
    }

    let listData = []
    if (props.listData) {
        listData = props.listData.map((item, idx) => {
            return (
                <tr key={idx}>
                    <th style={{ width: "100px" }}>{item.id}</th>
                    <th style={{ width: "100px" }}>{item.name}</th>
                    <th style={{ width: "100px" }}>{item.gender}</th>
                    <th><button onClick={() => {
                        setId(item._id)
                        setName(item.name)
                        setGender(item.gender)
                    }}>Sửa</button></th>
                    <th><button onClick={() => {
                        props.deleteItem(item._id)
                    }}>Xóa</button></th>
                </tr >
            )
        })
    }
    let listButton = []
    if (props.totalPage) {
        for (let i = 1; i <= props.totalPage; i++) {
            listButton.push(
                <button onClick={() => {
                    props.getItem({ page: i })
                }}
                    style={props.activePage === i ? { background: "red", color: "white" } : { background: "black", color: "white" }}
                >
                    {i}
                </button>
            )

        }
    }


    return (
        <div>
            <input type="file" name="uploadfile" onChange={saveFile} />
            <button onClick={uploadFile}>Tải</button>
            <br />
            <br />
            <input value={name} placeholder="nhập tên" onChange={(e) => {
                setName(e.target.value)
            }} />
            <input value={gender} placeholder="nhập giới tính" onChange={(e) => {
                setGender(e.target.value)
            }} />
            {
                id === 0
                    ?
                    <button onClick={() => {
                        if (!name || !gender) {
                            alert("nhập đủ tên và giới tính")
                        } else {
                            props.postItem({ id: Math.ceil(Math.random() * 100), name: name, gender: gender })
                        }
                    }}>Thêm</button>

                    :
                    <>
                        <button onClick={() => {
                            if (!name || !gender) {
                                alert("nhập đủ tên và giới tính")
                            } else {
                                props.putItem({ id, name, gender })
                            }
                        }}>Update</button>
                        <button onClick={() => {
                            setId(0)
                            setGender('')
                            setName('')
                        }}>Hủy Update</button>
                    </>
            }

            <br />
            <table>
                {listData}
            </table>
            {listButton}

            <br />
            <br />
            <br />
            <button onClick={exportFile}>Xuất Dữ Liệu</button>
        </div>
    )
}


export default FormComponent;