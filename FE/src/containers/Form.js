import React, { useState, useEffect } from 'react';
import FormComponent from '../components/FormComponent'
import { connect } from "react-redux"
import * as actions from '../actions/index';

const FormContainer = (props) => {
    useEffect(() => {
        props.getItem({ page: 1 })
    }, [])
    return (
        <div>
            <FormComponent {...props} />
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        listData: state.Form.listData,
        totalPage: state.Form.totalPage,
        activePage: state.Form.activePage,
    }
}
const mapDispatchToProps = (dispath) => {
    return {
        upload: (data) => {
            dispath(actions.uploadFileRequest(data))
        },
        getItem: (data) => {
            dispath(actions.getItemRequest(data))
        },
        postItem: (data) => {
            dispath(actions.postItemRequest(data))
        },
        putItem: (data) => {
            dispath(actions.putItemRequest(data))
        },
        deleteItem: (data) => {
            dispath(actions.deleteItemRequest(data))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FormContainer)